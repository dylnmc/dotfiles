#!/bin/bash

if [ ! -f /tmp/_kmp_ ]; then
    kmp='dvorak'
else
    if [[ $(cat /tmp/_kmp_) == 'us' ]]; then
        kmp='dvorak'
    else
        kmp='us'
    fi
fi

printf "$kmp"> /tmp/_kmp_

setxkbmap \
    -variant altgr-intl \
    -option \
    -option compose:rctrl \
    -option caps:ctrl_modifier \
    -option lv3:ralt_switch\
    "$kmp"

