(package-initialize)


(defface nord-blue-face '((t (:foreground "#2e3440" :background "#81a1c1"))) "")

;(setq telephone-line-faces
;      '((nord-blue . (nord-blue-face . nord-blue-face))
;        (accent . (telephone-line-accent-active . telephone-line-accent-inactive))
;        (nil . (mode-line . mode-line-inactive))))
	
;(setq telephone-line-primary-left-separator    'telephone-line-identity-left
;      telephone-line-primary-right-separator   'telephone-line-identity-left
;      telephone-line-secondary-left-separator  'telephone-line-nil
;      telephone-line-secondary-right-separator 'telephone-line-nil)

;(setq telephone-line-lhs
;      '((nord-blue . (telephone-line-minor-mode-segment
;                      telephone-line-buffer-segment))
;        (nil       . (telephone-line-vc-segment
;                      telephone-line-erc-modified-channels-segment
;                      telephone-line-process-segment))))
        
;(setq telephone-line-rhs
;      '((nil       . (telephone-line-misc-info-segment))
;        (nord-blue . (telephone-line-major-mode-segment))
;        (nil       . (telephone-line-airline-position-segment))))

;(telephone-line-mode t)


(helm-mode 1)
(global-set-key (kbd "M-x") 'helm-M-x)


(require 'package)
(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/"))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(blink-cursor-blinks 0)
 '(blink-cursor-mode nil)
 '(custom-enabled-themes (quote (nord)))
 '(custom-safe-themes
   (quote
    ("a27c00821ccfd5a78b01e4f35dc056706dd9ede09a8b90c6955ae6a390eb1c1e" "40da996f3246a3e99a2dff2c6b78e65307382f23db161b8316a5440b037eb72c" default)))
 '(inhibit-startup-screen t)
 '(menu-bar-mode nil)
 '(package-selected-packages
   (quote
    (haskell-mode which-key evil markdown-mode telephone-line helm nord-theme)))
 '(scroll-bar-mode nil)
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
