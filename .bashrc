[[ $- != *i* ]] && return

if [[ -f $HOME/.profile ]]; then
    source $HOME/.profile
fi

# don't echo control characters
stty -ctlecho
# turn off flow control
stty -ixon

# complete sudo commands
complete -cf sudo

# set up dircolors
eval $(dircolors ~/.dircolors)

if [[ $TERM == xterm-termite ]]; then
	. /etc/profile.d/vte.sh
	__vte_prompt_command
fi

# man in vim
function man(){
	if ! [[ -t 0 && -t 1 && -t 2 ]]; then
		command man "$@";
		return;
	fi;
	if [ $# -eq 0 ]; then
		echo 'What manual page do you want?';
		return 0;
	elif ! man -d "$@" &> /dev/null; then
		echo 'No manual entry for '"$*"
		return 1;
	fi;
	vim -c 'execute "normal! :let no_man_maps = 1\<cr>:runtime ftplugin/man.vim\<cr>:Man '"$*"'\<cr>:wincmd o\<cr>"';
}

# tmux aliases
alias tmux='tmux -2u'
alias tmnew='tmux new -s'
tmatt(){
    tmux attach -t $1 || tmnew $1
}

# exports
export HISTSIZE=10000
export HISTFILESIZE=${HISTSIZE}
export HISTCONTROL=ignoreboth
export JAVA_FONTS=/usr/share/fonts/TTF

# shoptions
shopt -s cdspell
shopt -s checkwinsize
shopt -s cmdhist
shopt -s dotglob
shopt -s expand_aliases
shopt -s extglob
shopt -s histappend
shopt -s hostcomplete

# hard screen clear
bind -x '"\C-t": printf "\ec"'

# tab
bind 'set show-all-if-ambiguous on'
bind '"\C-i" menu-complete'

# edit dot files function
function edlink(){
	[[ -z $1 ]] && return
	f=$(readlink -f $1)
	cd $(dirname $f)
	${EDITOR:-vi} $(basename $f)
	cd - > /dev/null
}
# dot file aliases
alias edbash='edlink $HOME/.bashrc; if [ "$(echo $BASH_VERSION)" ]; then source $HOME/.bashrc; fi'
alias edzsh='edlink $HOME/.zshrc; if [ "$(echo $ZSH_VERSION)" ]; then source $HOME/.zshrc; fi'
alias edvim='edlink $HOME/.vim/vimrc'
alias edgvim='edlink $HOME/.gvimrc'
alias ednvim='edlink $HOME/.config/nvim/init.vim'
alias edtmux='edlink $HOME/.tmux.conf'
alias edi3='edlink $HOME/.config/i3/config'
alias edi3status='edlink $HOME/.i3status.conf'
alias edi3blocks='edlink $HOME/.i3blocks.conf'
alias edtermite='edlink $HOME/.config/termite/config'
alias edxresources='edlink $HOME/.Xresources; xrdb $HOME/.Xresources'
alias edxinit='edlink $HOME/.xinitrc'
alias edbspwm='edlink $HOME/.config/bspwm/bspwmrc'
alias edsxhkd='edlink $HOME/.config/sxhkd/sxhkdrc'
alias eddunst='edlink $HOME/.config/dunst/dunstrc; killall dunst; (dunst &)'
alias edzprofile='edlink $HOME/.zprofile'
alias edprofile='edlink $HOME/.profile'
alias edbashprofile='edlink $HOME/.bash_profile'
alias edcompton='edlink $HOME/.config/compton.conf'
alias edpolybar='edlink $HOME/.config/polybar/config'
alias edgit='edlink $HOME/.gitconfig'
alias edranger='edlink $HOME/.config/ranger/rc.conf'

# cower color
alias cower='cower --color=auto'

# ls aliases
alias ls='ls --color=auto'
alias la='ls -AF'
alias ll='ls -lAF'

# grep options
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

# ps1
_ps1_dir_type=2
# source ~/git/promptly/powerline.sh
# source ~/git/promptly/grml.sh


# plain-text(ish) files to pdf
function file2pdf(){
    libreoffice --headless --invisible --norestore --convert-to pdf $@
}

# vim:fdm=marker
