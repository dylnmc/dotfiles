#!/bin/sh

if [[ -z $USER ]]; then
    USER="dylnmc";
fi;
pup="$(pacman -Qqu --dbpath /tmp/checkup-db-${USER}/ | wc -l)";
if (( pup > 0 )); then
   echo -en "$pup update(s)";
else
    echo "No updates";
fi;
